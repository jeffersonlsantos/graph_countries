import React, {useEffect} from 'react';
import CountryDetaislScreen from '../screens/countryDetails.screen';
import {useParams, useNavigate} from 'react-router-dom';
import {COUNTRY_DETAIL, COUNTRIES} from "../client/queries";
import {useApolloClient, gql} from '@apollo/client';
import * as Validacoes from '../utils/validations';

const CountryDetailsController = () => {

    const client = useApolloClient();
    const history = useNavigate();
    const {id} = useParams();
    let getCountryDetail = {};
    const pageName = 'Country Details';

    const getCacheCountries = client.readQuery({
        query: COUNTRIES
    });

    useEffect(() => {
        if (client.cache.watches.size < 1) {
            history('/countries');
            return false;
        }
    }, [client.cache.watches.size, history]);

    if (Validacoes.isNotNullOrUndefinedOrEmpty(getCacheCountries)) {
        getCountryDetail = getCacheCountries.Country.find(country => {
            return country._id === id;
        });
    }

    const handlers = {
        getCountryDetail,
        pageName
    }

    return <CountryDetaislScreen handlers={handlers}/>
}

export default CountryDetailsController;
