import React, {useCallback, useEffect, useState} from 'react';
import {COUNTRIES, GET_FORM_INPUTS} from "../client/queries";
import CardListScreen from '../screens/cardList.screen';
import {useQuery, useReactiveVar} from "@apollo/client";
import { countryNameSearchVar, isCachedCountryVar } from "../client/state/localVariables";
import * as Validacoes from '../utils/validations';
import Grid from "@mui/material/Grid";
import GraphCountriesCard from "../components/layout/graphCountriesCard.component";

const CardListController = () => {

    const { data, loading, error } = useQuery(GET_FORM_INPUTS);
    const countryName = useReactiveVar(countryNameSearchVar);
    const isCachedCountry = useReactiveVar(isCachedCountryVar);
    const pageName = 'Countries';

    const handleSubmit = (event, inputSearchCountryName) => {
        event.preventDefault();
        countryNameSearchVar(inputSearchCountryName);
        isCachedCountryVar(true);
    };

    const handlers = {
        COUNTRIES,
        pageName,
        handleSubmit,
        countryName,
        isCachedCountry,
        data,
        loading,
        error
    }

    return <CardListScreen handlers={handlers}/>
}

export default CardListController;
