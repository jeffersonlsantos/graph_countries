import React, {useEffect, useState} from 'react';
import CountryEditScreen from '../screens/countryEdit.screen';
import {useParams, useNavigate} from 'react-router-dom';
import {COUNTRIES} from "../client/queries";
import {useApolloClient, useReactiveVar} from '@apollo/client';
import * as Validacoes from '../utils/validations';
import {
    countryNameSearchVar,
    inputCountryAreaVar,
    inputCountryCapitalVar,
    inputCountryFlagIdVar,
    inputCountryFlagVar,
    inputCountryNameVar,
    inputCountryPopulationVar,
    inputCountryTopLevelDomainsVar
} from "../client/state/localVariables";

const CountryEditController = () => {

    const inputCountryName = useReactiveVar(inputCountryNameVar);
    const inputCountryFlag = useReactiveVar(inputCountryFlagVar);
    const inputCountryFlagId = useReactiveVar(inputCountryFlagIdVar);
    const inputCountryCapital = useReactiveVar(inputCountryCapitalVar);
    const inputCountryArea = useReactiveVar(inputCountryAreaVar);
    const inputCountryPopulation = useReactiveVar(inputCountryPopulationVar);
    const inputCountryTopLevelDomains = useReactiveVar(inputCountryTopLevelDomainsVar);

    const client = useApolloClient();
    const history = useNavigate();
    const {id} = useParams();
    let getCountryToEdit = {};
    const pageName = `Country Edit: ${inputCountryName}`;

    const [snackBarState, setSnackBarState] = useState({
        open: false,
        vertical: 'bottom',
        horizontal: 'left',
    });

    const handleSnackBar = (newState) => {
        setSnackBarState({open: true, ...newState});
    };

    const handleSnackBarClose = () => {
        setSnackBarState({...snackBarState, open: false});
    };

    useEffect(() => {
        if (client.cache.watches.size < 1) {
            history('/countries');
            return false;
        }
    }, [client.cache.watches.size, history]);

    const getCountryInputs = () => {
        const getCacheCountries = client.readQuery({
            query: COUNTRIES
        });

        if (Validacoes.isNotNullOrUndefinedOrEmpty(getCacheCountries)) {
            getCountryToEdit = getCacheCountries.Country.find(country => {
                return country._id === id;
            });
            inputCountryNameVar(getCountryToEdit.name);
            inputCountryFlagVar(getCountryToEdit.flag.svgFile);
            inputCountryFlagIdVar(getCountryToEdit.flag._id);
            inputCountryCapitalVar(getCountryToEdit.capital);
            inputCountryAreaVar(getCountryToEdit.area);
            inputCountryPopulationVar(getCountryToEdit.population);
            inputCountryTopLevelDomainsVar(getCountryToEdit.topLevelDomains[0].name);
        }
    }

    const handleSubmit = (event, values) => {
        event.preventDefault();

        const countries = client.readQuery({ query: COUNTRIES });

        const index = countries.Country.findIndex((element, index) => {
            if (element._id === id) {
                return true
            }
        });

        const editCountry = {
            _id: id,
            __typename: 'Country',
            name: values.inputName??inputCountryName,
            capital: values.inputCapital??inputCountryCapital,
            area: values.inputArea??inputCountryArea,
            population: values.inputPopulation??inputCountryPopulation,
            topLevelDomains: [{
                name: values.inputTopLevelDomains??inputCountryTopLevelDomains,
            }],
            flag: {
                _id: inputCountryFlagId,
                __typename: "Flag",
                svgFile: values.inputUrlFlag??inputCountryFlag,
            },
        };

        const cacheData = [...countries.Country];

        cacheData[index] = editCountry;

        client.writeQuery({
            query: COUNTRIES,
            data: {
                Country: cacheData
            },
        });

        countryNameSearchVar('');
        handleSnackBar({
          vertical: 'bottom',
          horizontal: 'left',
        })
    }

    const handlers = {
        id,
        snackBarState,
        pageName,
        handleSnackBar,
        handleSnackBarClose,
        handleSubmit,
        getCountryInputs,
    }

    return <CountryEditScreen handlers={handlers}/>
}

export default CountryEditController;
