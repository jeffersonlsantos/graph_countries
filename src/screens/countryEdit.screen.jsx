import React, {useEffect, useState, forwardRef} from 'react';
import GraphCountriesLayout from "../components/layout/graphCountriesLayout.component";
import Box from "@mui/material/Box";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Grid from "@mui/material/Grid";
import styled from "@emotion/styled";
import Container from "@mui/material/Container";
import {CardActions, Snackbar} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useReactiveVar} from "@apollo/client";
import MuiAlert from '@mui/material/Alert';
import {
    inputCountryAreaVar,
    inputCountryCapitalVar,
    inputCountryFlagIdVar,
    inputCountryFlagVar,
    inputCountryNameVar, inputCountryPopulationVar, inputCountryTopLevelDomainsVar
} from "../client/state/localVariables";
import {InputText} from '../components/inputs/graphCountriesInputText.component';

const CountryDetailScreen = ({handlers}) => {

    const inputCountryName = useReactiveVar(inputCountryNameVar);
    const inputCountryFlag = useReactiveVar(inputCountryFlagVar);
    const inputCountryFlagId = useReactiveVar(inputCountryFlagIdVar);
    const inputCountryCapital = useReactiveVar(inputCountryCapitalVar);
    const inputCountryArea = useReactiveVar(inputCountryAreaVar);
    const inputCountryPopulation = useReactiveVar(inputCountryPopulationVar);
    const inputCountryTopLevelDomains = useReactiveVar(inputCountryTopLevelDomainsVar);

    const {
        id,
        snackBarState,
        pageName,
        handleSnackBarClose,
        handleSubmit,
        getCountryInputs,
    } = handlers;

    useEffect(() => {
        getCountryInputs();
    }, [getCountryInputs]);

    const [values, setValues] = useState({
         inputId: id,
         inputFlagId: undefined,
         inputUrlFlag: undefined,
         inputName: undefined,
         inputCapital: undefined,
         inputArea: undefined,
         inputPopulation: undefined,
         inputTopLevelDomains: undefined,
    });

    const { vertical, horizontal, open }  = snackBarState

    const handleChange = (prop) => (event) => {
         setValues({
            ...values,
            [prop]: event.target.value
        });
    };

    const Alert = React.forwardRef(function Alert(props, ref) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
    });

    return (
        <GraphCountriesLayout pageName={pageName}>
            <Snackbar
                open={open}
                onClose={handleSnackBarClose}
                autoHideDuration={4000}
                anchorOrigin={{ vertical, horizontal }}
                key={vertical + horizontal}
            >
                <Alert onClose={handleSnackBarClose} severity="success" sx={{width: '100%'}}>
                    Dados alterados!
                </Alert>
            </Snackbar>
            <div style={{width: '100%'}}>
                <Container maxWidth="md" sx={{mt: 10, mb: 4}}>
                    <Card>
                        <form onSubmit={event => handleSubmit(event, values)}>
                            <CardContent>
                                <Grid container spacing={3}>
                                    <Grid item xs={12}>
                                        <Box>
                                            <div>
                                                <InputText
                                                    id={"country-flag"}
                                                    label={"Digite a URL da Bandeira do País"}
                                                    name={"country-flag"}
                                                    type={"text"}
                                                    placeholder={"Digite a URL da Bandeira do País"}
                                                    onInputValido={handleChange("inputUrlFlag")}
                                                    mensagemExcessao={"Digite o endereço da bandeira"}
                                                    habilitado={true}
                                                    valorInicial={inputCountryFlag}
                                                    obrigatorio={true}
                                                />
                                            </div>
                                        </Box>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Box>
                                            <div>
                                                <InputText
                                                    id={"country-name"}
                                                    label={"Digite nome do País"}
                                                    name={"country-name"}
                                                    type={"text"}
                                                    placeholder={"Digite o nome País"}
                                                    onInputValido={handleChange("inputName")}
                                                    mensagemExcessao={"Digite o nome País"}
                                                    habilitado={true}
                                                    valorInicial={inputCountryName}
                                                    obrigatorio={true}
                                                />
                                            </div>
                                        </Box>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Box>
                                            <div>
                                                <InputText
                                                    id={"country-capital"}
                                                    label={"Digite nome da Capital do País"}
                                                    name={"country-capital"}
                                                    type={"text"}
                                                    placeholder={"Digite nome da Capital do País"}
                                                    onInputValido={handleChange("inputCapital")}
                                                    mensagemExcessao={"Digite nome da Capital do País"}
                                                    habilitado={true}
                                                    valorInicial={inputCountryCapital}
                                                    obrigatorio={true}
                                                />
                                            </div>
                                        </Box>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Box>
                                            <div>
                                                <InputText
                                                    id={"country-area"}
                                                    label={"Digite a Área Total do País"}
                                                    name={"country-area"}
                                                    type={"number"}
                                                    placeholder={"Digite a Área Total do País"}
                                                    onInputValido={handleChange("inputArea")}
                                                    mensagemExcessao={"Digite a Área Total do País"}
                                                    habilitado={true}
                                                    valorInicial={inputCountryArea}
                                                    obrigatorio={true}
                                                />
                                            </div>
                                        </Box>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Box>
                                            <div>
                                                <InputText
                                                    id={"country-population"}
                                                    label={"Digite a População Total do País"}
                                                    name={"country-population"}
                                                    type={"number"}
                                                    placeholder={"Digite a População Total do País"}
                                                    onInputValido={handleChange("inputPopulation")}
                                                    mensagemExcessao={"Digite a População Total do País"}
                                                    habilitado={true}
                                                    valorInicial={inputCountryPopulation}
                                                    obrigatorio={true}
                                                />
                                            </div>
                                        </Box>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Box>
                                            <div>
                                                <InputText
                                                    id={"country-top-level-domains"}
                                                    label={"Digite o Top Level Domain do País"}
                                                    name={"country-top-level-domains"}
                                                    type={"text"}
                                                    placeholder={"Digite o Top Level Domain do País"}
                                                    onInputValido={handleChange("inputCountryTopLevelDomains")}
                                                    mensagemExcessao={"Digite o Top Level Domain do País"}
                                                    habilitado={true}
                                                    valorInicial={inputCountryTopLevelDomains}
                                                    obrigatorio={true}
                                                />
                                            </div>
                                        </Box>
                                    </Grid>
                                </Grid>
                            </CardContent>
                            <CardActions>
                                <Grid container
                                      direction="row"
                                      justifyContent="space-between"
                                      alignItems="center"
                                >
                                    <Grid item>
                                        <Button
                                            type={'submit'}
                                            variant="contained"
                                            color={'success'}
                                            size="large"
                                        >
                                            Editar
                                        </Button>
                                    </Grid>
                                    <Grid item>
                                        <StyledLink to={'/countries'}>
                                            <Button variant="contained" size="large">VOLTAR</Button>
                                        </StyledLink>
                                    </Grid>
                                </Grid>
                            </CardActions>
                        </form>
                    </Card>
                </Container>
            </div>
        </GraphCountriesLayout>
    );
}

export default CountryDetailScreen;

const StyledLink = styled(Link)`
    text-decoration: none;
    &:focus, &:hover, &:visited, &:link, &:active {
        text-decoration: none;
    }
`;