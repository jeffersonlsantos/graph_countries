import React, {useState} from 'react';
import GraphCountriesLayout from "../components/layout/graphCountriesLayout.component";
import GraphCountriesCard from "../components/layout/graphCountriesCard.component";
import IconButton from '@mui/material/IconButton';
import Input from '@mui/material/Input';
import FilledInput from '@mui/material/FilledInput';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import * as Validacoes from '../utils/validations';
import Button from '@mui/material/Button';
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import CardCountries from '../components/layout/graphCountriesCard.component';
import {InputText} from "../components/inputs/graphCountriesInputText.component";

const CardListScreen = ({handlers}) => {

    const {handleSubmit, pageName, countryName, isCachedCountry} = handlers;
    const [inputSearchCountryName, setCountryName] = useState('');

    const handleChange = (event) => {
        setCountryName(event.target.value);
    };

    return (
        <GraphCountriesLayout pageName={pageName}>
            <div style={{width: '100%'}}>
                <Box
                    sx={{
                        gap: 3,
                        gridTemplateColumns: 'repeat(3, 1fr)',
                    }}
                >
                    <form onSubmit={event => handleSubmit(event, inputSearchCountryName)}>
                        <Grid
                            container
                            direction="row"
                            justifyContent="space-between"
                            alignItems="center"
                        >
                            <Box>
                                <Grid item xs={12}>

                                        <InputText
                                            id={"country-search"}
                                            label={"Digite o nome do País"}
                                            name={"country-search"}
                                            type={"text"}
                                            placeholder={"Digite o nome do País"}
                                            onInputValido={handleChange}
                                            mensagemExcessao={"Digite o nome do País"}
                                            habilitado={true}
                                            valorInicial={inputSearchCountryName}
                                            obrigatorio={false}
                                        />

                                </Grid>
                            </Box>

                            <Box>
                                <div>
                                    <Button
                                        type={'submit'} variant="contained"
                                        color={'success'}
                                        size="large"
                                    >
                                        BUSCAR
                                    </Button>
                                </div>
                            </Box>
                        </Grid>
                    </form>
                    <br/>
                    <br/>
                    <Grid container spacing={3}>
                        <CardCountries name={countryName} cache={isCachedCountry}/>
                    </Grid>
                </Box>
            </div>
        </GraphCountriesLayout>
    )
        ;
}

export default CardListScreen;