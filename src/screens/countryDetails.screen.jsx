import React from 'react';
import GraphCountriesLayout from "../components/layout/graphCountriesLayout.component";
import GraphCountryDetail from "../components/layout/graphCountriesDetail.component"

const CountryDetailScreen = ({handlers}) => {

    return (
        <GraphCountriesLayout pageName={handlers.pageName}>
            <GraphCountryDetail props={handlers} />
        </GraphCountriesLayout>
    );
}

export default CountryDetailScreen;