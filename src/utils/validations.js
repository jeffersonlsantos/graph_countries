import PropTypes from "prop-types";
import {Regex} from './constants';

export const allIsNotNullOrUndefinedOrEmpty = (params) => {
    allIsNotNullOrUndefinedOrEmpty.propTypes = {
        params: PropTypes.array.isRequired,
    };
    if(params && params.length > 0) {
        return params.every(isNotNullOrUndefinedOrEmpty);
    }
    return false;
}

export const isNotNullOrUndefinedOrEmpty = (p) => {
    isNotNullOrUndefinedOrEmpty.propTypes = {
        p: PropTypes.any.isRequired,
    };
    return  p !== undefined && p != null && (typeof(p) === 'string' ? p !== "" && p !== " " : true);
}

export const isNotNullOrUndefinedOrEmptyArray = (p) => {
    isNotNullOrUndefinedOrEmpty.propTypes = {
        p: PropTypes.array.isRequired,
    };
    return p !== undefined && p != null && p.length > 0;
}

export const ValidarTexto = (textoAlfa, minLength = '', maxLength = '') => {
    const minMaxValue = ValidarMaxMinValor(textoAlfa, minLength, maxLength);
    if (!minMaxValue) {
        return false;
    }
    let regex = Regex.texto;
    return ValidarViaRegex(regex, textoAlfa);
}

export const ValidarNumero = (numero) => {
    let regex = Regex.numeros;
    return ValidarViaRegex(regex, numero);
}

export const ValidarMaxMinValor = (valor, minLength = '', maxLength = '') => {
    if (minLength !== '') {
        if (valor.length < minLength) {
            return false;
        }
    }
    if (maxLength !== '') {
        if (valor.length > maxLength) {
            return false;
        }
    }
    return true;
}

export const ValidarViaRegex = (strRegex, strValue) => {
    let status = isNotNullOrUndefinedOrEmpty(strValue);
    if (status) {
        status = strRegex.test(strValue.toLowerCase());
    }
    return status;
}