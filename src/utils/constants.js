export const Regex = {
    apenasDigitos: /[^0-9]/g,
    letrasNumeros: /[a-zA-Z0-9]/,
    texto: /[A-Za-zÀ-ú0-9\s]/,
    numeros: /(^[0-9]+)[\s]?([a-zA-Z])*$/gi,
}
