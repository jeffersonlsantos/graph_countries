import { makeVar } from '@apollo/client';

export const isCachedCountryVar = makeVar(false);
export const countryNameSearchVar = makeVar('');
export const inputCountryNameVar = makeVar('');
export const inputCountryFlagVar = makeVar('');
export const inputCountryFlagIdVar = makeVar('');
export const inputCountryCapitalVar = makeVar('');
export const inputCountryAreaVar = makeVar('');
export const inputCountryPopulationVar = makeVar('');
export const inputCountryTopLevelDomainsVar = makeVar('');

