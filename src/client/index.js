import {ApolloClient, InMemoryCache} from "@apollo/client";
import {
    countryNameSearchVar,
    inputCountryAreaVar,
    inputCountryCapitalVar,
    inputCountryFlagVar,
    inputCountryFlagIdVar,
    inputCountryNameVar,
    inputCountryPopulationVar,
    inputCountryTopLevelDomainsVar
} from "./state/localVariables";

const client = new ApolloClient({
    uri: 'http://testefront.dev.softplan.com.br/',
    cache: new InMemoryCache({
        typePolicies: {
            Query: {
                fields: {
                    inputCountryName: {
                        read() {
                            return inputCountryNameVar();
                        }
                    },
                    countryNameSearch: {
                        read() {
                            return countryNameSearchVar();
                        }
                    },
                    inputCountryFlag: {
                        read() {
                            return inputCountryFlagVar();
                        }
                    },
                    inputCountryFlagId: {
                        read() {
                            return inputCountryFlagIdVar();
                        }
                    },
                    inputCountryCapital: {
                        read() {
                            return inputCountryCapitalVar();
                        }
                    },
                    inputCountryPopulation: {
                        read() {
                            return inputCountryPopulationVar();
                        }
                    },
                    inputCountryTopLevelDomains: {
                        read() {
                            return inputCountryTopLevelDomainsVar();
                        }
                    },
                    inputCountryArea: {
                        read() {
                            return inputCountryAreaVar();
                        }
                    },
                }
            },
            Country: {
                keyFields: ["_id", "name"],
                fields: {
                    capital: {
                        merge: true,
                    },
                },
            },
        }
    })
});

export default client;