import { gql } from '@apollo/client';

export const COUNTRIES = gql`
    query GetCountries($name: String) {
        Country(name: $name) {
           _id
          name
          capital
          area
          population
          topLevelDomains {
            name
          }
          flag {
            _id
            svgFile
          }  
        }
    }
`;

export const COUNTRY_DETAIL = gql`
  query {
      Country {
          _id
          name
          capital
          area
          population
          topLevelDomains {
            name
          }
          flag {
            svgFile
          }  
      }
  }
`;

export const GET_FORM_INPUTS = gql`
  query GetFormInputs {
    inputCountryName @client
    inputCountryFlag @client
    inputCountryFlagId @client
    inputCountryCapital @client
    inputCountryPopulation @client
    inputCountryTopLevelDomains @client
    inputCountryArea @client
  }
`;