import {
    BrowserRouter as Router,
    Routes,
    Route,
    Navigate,
} from 'react-router-dom';

import CardListController from '../controllers/cardList.controller';
import CountryDetailsController from "../controllers/countryDetails.controller";
import CountryEditController from "../controllers/countryEdit.controller";

const SEFRouter = () => {
    return (
        <Router>
            <Routes>
                <Route exact path={"/"} element={<Navigate to="/countries"/>}/>
                <Route exact path={"/countries"} element={<CardListController/>}/>
                <Route exact path={"/country-details/:id"} element={<CountryDetailsController/>}/>
                <Route exact path={"/country-edit/:id"} element={<CountryEditController/>}/>
            </Routes>
        </Router>
    )
};

export default SEFRouter;