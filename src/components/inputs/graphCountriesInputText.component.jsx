import React, {useState, useEffect} from 'react';
import PropTypes from "prop-types";
import {TextField} from "@material-ui/core";
import * as Validacoes from '../../utils/validations';

export const InputText = ({
	  id,
	  label,
	  name,
	  type,
	  placeholder,
	  onInputValido,
	  mensagemExcessao,
	  habilitado = true,
	  valorInicial = '',
	  obrigatorio = false,
	  autofocus = false,
  }) => {
	InputText.propTypes = {
		id: PropTypes.string.isRequired,
		label: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
		type: PropTypes.string.isRequired,
		placeholder: PropTypes.string,
		onInputValido: PropTypes.func,
		habilitado: PropTypes.bool,
		valorInicial: PropTypes.any,
		mensagemExcessao: PropTypes.string,
		obrigatorio: PropTypes.bool,
		autofocus: PropTypes.bool,
		maxLength: PropTypes.number,
	}

	const [inputValue, setInputValue] = useState(valorInicial);
	const [inputValido, setInputValido] = useState(null);

	const textChangehandler = (e) => {
		let isValido;
		isValido = Validacoes.ValidarTexto(e.target.value);
		setInputValue(e.target.value);
		setInputValido(isValido);
		if (onInputValido !== undefined) {
			onInputValido(e);
		}
	}

	useEffect(() => {
		setInputValue(valorInicial);
		setInputValido(!(obrigatorio && !Validacoes.isNotNullOrUndefinedOrEmpty(valorInicial)));
	}, [obrigatorio, valorInicial]);

	const mostrarErro = inputValido === null ? null : !inputValido;
	const helperText = mostrarErro ? mensagemExcessao : null;

	return (
		<TextField
			value={inputValue}
			onChange={textChangehandler}
			error={mostrarErro}
			helperText={helperText}
			InputLabelProps={{shrink: true}}
			placeholder={placeholder}
			variant="outlined"
			required={obrigatorio}
			disabled={!habilitado}
			fullWidth
			id={id}
			label={label}
			name={name}
			type={type}
			autoFocus={autofocus}
		/>
	);
}