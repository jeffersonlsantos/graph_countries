import * as React from 'react';
import {Link} from "react-router-dom";
import ListItem from '@mui/material/ListItem';
import FlagIcon from '@mui/icons-material/Flag';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import styled from "@emotion/styled";

const StyledLink = styled(Link)`
    text-decoration: none;
    &:focus, &:hover, &:visited, &:link, &:active {
        text-decoration: none;
    }
`;

export const mainListItems = (
    <div>
        <StyledLink to={'/countries'}>
            <ListItem button style={{textDecoration: 'none', color: '#343a40'}}>
                <ListItemIcon>
                    <FlagIcon/>
                </ListItemIcon>
                <ListItemText primary="Countries"/>
            </ListItem>
        </StyledLink>
    </div>
);