import * as React from 'react';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import GraphCountriesHeader from "./graphCountriesHeader.component";
import GraphCountriesBody from "./graphCountiesBody.component";

const mdTheme = createTheme();

const GraphCountriesLayout = ({pageName, children}) => {
    return (
        <ThemeProvider theme={mdTheme}>
            <Box sx={{display: 'flex'}}>
                <CssBaseline/>
                <GraphCountriesHeader pageName={pageName} />
                <GraphCountriesBody>
                    {children}
                </GraphCountriesBody>
            </Box>
        </ThemeProvider>
    );
}

export default GraphCountriesLayout;