import React, {useState} from 'react';
import {useQuery, useApolloClient} from '@apollo/client';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

import Grid from "@mui/material/Grid";
import {Link} from "react-router-dom";
import styled from "@emotion/styled";
import {COUNTRIES} from "../../client/queries";
import * as Validacao from '../../utils/validations';

const GraphCountriesCard = ({props}) => {
    return (
        <Card sx={{width: 345}}>
            <CardMedia
                component="img"
                alt={props.name}
                height="140"
                image={props.flag.svgFile}
            />
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                    {props.name}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    {`Capital: ${props.capital}`}
                </Typography>
            </CardContent>
            <CardActions>
                <Grid container
                      direction="row"
                      justifyContent="space-between"
                      alignItems="center"
                >
                    <Grid item>
                        <StyledLink to={`/country-details/${props._id}`}>
                            <Button variant="contained" size="medium">DETALHES</Button>
                        </StyledLink>
                    </Grid>
                    <Grid item>
                        <StyledLink to={`/country-edit/${props._id}`}>
                            <Button variant="contained" color={'warning'} size="medium">EDITAR</Button>
                        </StyledLink>
                    </Grid>
                </Grid>
            </CardActions>
        </Card>
    );
}

const CardCountries = ({name, cache}) => {

    const client = useApolloClient();

    const searchCountryByName = useQuery(COUNTRIES, {
        variables: {name}
    });
    const searchCountry = useQuery(COUNTRIES);

    const searchCountryCache = client.readQuery({query: COUNTRIES});

    if (cache) {
        if (Validacao.isNotNullOrUndefinedOrEmpty(name)) {
            const query = searchCountryCache.Country.find(query => {

                return query.name === name;
            });

            return (
                <Grid item key={query._id}>
                    <GraphCountriesCard props={query}/>
                </Grid>
            )
        }

        return searchCountryCache.Country.map(country => (
            <Grid item key={country._id}>
                <GraphCountriesCard props={country}/>
            </Grid>
        ))
    } else {
        const {
            data,
            loading,
            error
        } = Validacao.isNotNullOrUndefinedOrEmpty(name) ? searchCountryByName : searchCountry;

        if (loading) return <Grid item><p>Loading...</p></Grid>
        if (error) return <Grid item><p>Error :(</p></Grid>;

        return data.Country.map(country => (
            <Grid item key={country._id}>
                <GraphCountriesCard props={country}/>
            </Grid>
        ))
    }
}

const StyledLink = styled(Link)`
    text-decoration: none;
    &:focus, &:hover, &:visited, &:link, &:active {
        text-decoration: none;
    }
`;

export default CardCountries;