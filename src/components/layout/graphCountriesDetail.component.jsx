import React from 'react';
import {Link} from "react-router-dom";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Avatar from '@mui/material/Avatar';
import LanguageIcon from '@mui/icons-material/Language';
import FlagIcon from '@mui/icons-material/Flag';
import AssessmentIcon from '@mui/icons-material/Assessment';
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import HttpIcon from '@mui/icons-material/Http';
import styled from "@emotion/styled";
import Container from "@mui/material/Container";
import ListItemText from '@mui/material/ListItemText';
import CardActions from '@mui/material/CardActions';
import ListItemAvatar from '@mui/material/ListItemAvatar';

const GraphCountryDetail = ({props}) => {

    const CardCountryItem = ({countryDetail}) => {

        const {
            _id,
            name,
            flag,
            capital,
            area,
            population,
            topLevelDomains
        } = countryDetail.getCountryDetail;

        return (
            <Card sx={{width: 345}}>
                <CardMedia
                    component="img"
                    alt={name}
                    height="140"
                    image={flag.svgFile}
                />
                <CardContent>
                    <List sx={{width: '100%', maxWidth: 360, bgcolor: 'background.paper'}}>
                        <ListItem>
                            <ListItemAvatar>
                                <Avatar>
                                    <FlagIcon/>
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary="Nome" secondary={name}/>
                        </ListItem>
                        <ListItem>
                            <ListItemAvatar>
                                <Avatar>
                                    <LanguageIcon/>
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary="Capital" secondary={capital}/>
                        </ListItem>
                        <ListItem>
                            <ListItemAvatar>
                                <Avatar>
                                    <AssessmentIcon/>
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary="Área" secondary={area}/>
                        </ListItem>
                        <ListItem>
                            <ListItemAvatar>
                                <Avatar>
                                    <PeopleAltIcon/>
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary="População" secondary={population}/>
                        </ListItem>
                        <ListItem>
                            <ListItemAvatar>
                                <Avatar>
                                    <HttpIcon/>
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary="Top Level Domain" secondary={topLevelDomains[0].name}/>
                        </ListItem>
                    </List>
                </CardContent>
                <CardActions>
                    <Grid container
                          direction="row"
                          justifyContent="space-between"
                          alignItems="center"
                    >
                        <Grid item>
                            <StyledLink to={'/countries'}>
                                <Button variant="contained" size="medium">VOLTAR</Button>
                            </StyledLink>
                        </Grid>
                        <Grid item>
                            <StyledLink to={`/country-edit/${_id}`}>
                                <Button variant="contained" color={'warning'} size="medium">EDITAR</Button>
                            </StyledLink>
                        </Grid>
                    </Grid>
                </CardActions>
            </Card>
        );
    }

    const CardCountryDetail = ({props}) => {

        return (
            <div style={{width: '100%'}}>
                <Box
                    sx={{
                        gap: 3,
                        gridTemplateColumns: 'repeat(3, 1fr)',
                    }}
                >
                    <Grid container spacing={3}>
                        <CardCountryItem countryDetail={props} />
                    </Grid>
                </Box>
            </div>
        );
    }

    return (
        <Container maxWidth="lg" sx={{mt: 10, mb: 4}}>
            <Grid container spacing={5}>
                <Grid
                    container
                    spacing={5}
                    direction="column"
                    alignItems="center"
                    justifyContent="center"
                >
                    <Grid item xs={12}>
                        <CardCountryDetail props={props}/>
                    </Grid>
                </Grid>
            </Grid>
        </Container>
    );
}

const StyledLink = styled(Link)`
    text-decoration: none;
    &:focus, &:hover, &:visited, &:link, &:active {
        text-decoration: none;
    }
`;

export default GraphCountryDetail;